import React from 'react';
import {Text, View, TextInput, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

const Header = (props) => {
	const {queTypeText, colorStyle} = props;
	return (
		<View>
			<View style = {styles.headerStyle}>
				<TouchableOpacity style = {{marginLeft: 5}}>
					<Icon name='menu' size={20} color='#ffffff'/>
				</TouchableOpacity>

				<Text style = {{textAlign: 'center', color: '#ffffff', fontSize: 20}}>Unit: Adjective</Text>

				<TouchableOpacity style = {{marginRight: 5}}>
					<Icon name='user' size={20} color='#ffffff'/>
				</TouchableOpacity>
			</View>

			<View style = {[styles.queTypeViewStyle, colorStyle]}>
				<Text style = {styles.queTypeStyle}>{queTypeText}</Text>
			</View>

		</View>

	);
}

const styles = {
	headerStyle: {
		// flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: 20,
		height: 30
	},
	queTypeViewStyle: {
		// flex: 1,
		alignSelf: 'stretch',
		
		justifyContent: 'center',
		alignItems: 'center',
		
		borderWidth: 1,
		borderRadius: 15,
		marginTop: 20,
		marginBottom: 20,
		marginLeft: 5,
		marginRight: 5,
		height: 50
	},
	queTypeStyle: {
		color: '#ffffff',
		fontSize: 20,
		textAlign: 'center'
	}
}


export default Header;