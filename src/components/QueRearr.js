import React, { Component } from 'react';
import {Text, View, TextInput, TouchableOpacity, Image, ToastAndroid} from 'react-native';
import Header from './Header';
import Footer from './Footer';
import { Actions } from 'react-native-router-flux';

class QueRearr extends Component {
	state = {
		label: 0,
		text: ''
	};

	labelIncrement(labelInc, textInc){
		if (this.state.label==labelInc-1 && labelInc==7){
			this.setState({label: labelInc, text: this.state.text+textInc})
			Actions.queMic();
		}
		if (this.state.label==labelInc-1){
			this.setState({label: labelInc, text: this.state.text+textInc})
		}
		else {
			ToastAndroid.show('Wrong Sequence', ToastAndroid.SHORT)
		}
		
	}

	render(){
		return (
			<View style = {styles.viewStyle}>
				<Header queTypeText = {'Rearrange the words to form a sentence.'} colorStyle = {styles.queTypeColorStyle}/>
				<View style = {{alignItems: 'center'}}>

					<View style = {styles.ansViewStyle}>
						<View style = {styles.ansLineStyle}>
							<TouchableOpacity style = {styles.buttonStyle} onPress = {() => this.labelIncrement(7, 'dog ')}>
								<Text style = {styles.buttonTextStyle}>dog</Text>
							</TouchableOpacity>
							<TouchableOpacity style = {styles.buttonStyle} onPress = {() => this.labelIncrement(1, 'Rahul ')}>
								<Text style = {styles.buttonTextStyle}>Rahul</Text>
							</TouchableOpacity>
							<TouchableOpacity style = {styles.buttonStyle} onPress = {() => this.labelIncrement(4, 'with ')}>
								<Text style = {styles.buttonTextStyle}>with</Text>
							</TouchableOpacity>
							<TouchableOpacity style = {styles.buttonStyle} onPress = {() => this.labelIncrement(5, 'his ')}>
								<Text style = {styles.buttonTextStyle}>his</Text>
							</TouchableOpacity>
						</View>
						<View style = {styles.ansLineStyle}>
							<TouchableOpacity style = {styles.buttonStyle} onPress = {() => this.labelIncrement(3, 'playing ')}>
								<Text style = {styles.buttonTextStyle}>playing</Text>
							</TouchableOpacity>
							<TouchableOpacity style = {styles.buttonStyle} onPress = {() => this.labelIncrement(2, 'enjoys ')}>
								<Text style = {styles.buttonTextStyle}>enjoys</Text>
							</TouchableOpacity>
							<TouchableOpacity style = {styles.buttonStyle} onPress = {() => this.labelIncrement(6, 'pet ')}>
								<Text style = {styles.buttonTextStyle}>pet</Text>
							</TouchableOpacity>
						</View>
					</View>
					<View style = {styles.queViewStyle}>
						<Text style = {styles.queStyle}> {this.state.text} </Text>
					</View>
				</View>

				<Footer />
			</View>
		);
	}
};

const styles = {
	viewStyle: {
		fontSize: 18,
		flex: 1,
	    backgroundColor: '#3b8168',
	    color: '#ffffff'
	},
	queTypeColorStyle: {
		backgroundColor: '#065b6d',
		borderColor: '#065b6d',
	},
	queViewStyle: {
		marginLeft: 20,
		marginRight: 20,
		height: 100,
		width: 300,
		borderWidth: 1,
		borderColor: '#3b8168',
		backgroundColor: '#3b8168' 
	},
	queStyle: {
		fontSize: 25,
		color: '#ffffff',
	},
	ansViewStyle: {
		flexDirection: 'column',
		justifyContent: 'space-between',
		marginTop: 60
	},
	ansLineStyle: {
		flexDirection: 'row',
		marginBottom: 20
	},
	buttonStyle: {
		backgroundColor: '#065b6d',
		borderWidth: 1,
		borderColor: '#ffffff',
		borderRadius: 10,
		height: 40,
		width: 70,
		marginLeft: 5,
		marginRight: 5,
		padding: 0
	},
	buttonTextStyle: {
		fontSize: 20,
		color: '#ffffff',
		padding: 2,
		textAlign: 'center'
	}
};

export default QueRearr;