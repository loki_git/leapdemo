import React, { Component } from 'react';
import {Text, View, TextInput, TouchableOpacity, Image, ToastAndroid, Alert} from 'react-native';
import Header from './Header';
import Footer from './Footer';
import Icon from 'react-native-vector-icons/Entypo';
import {AudioRecorder, AudioUtils} from 'react-native-audio';
import { PermissionsAndroid } from 'react-native';
import SoundRecorder from 'react-native-sound-recorder';

class QueMic extends Component {

	async requestRecorderPermission(){
			try {
			    const granted = await PermissionsAndroid.request(
			      PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
			      {
			        'title': 'LeapTool Permission',
			        'message': 'LeapTool App needs access to your Recorder ' +
			                   'so you can record audio.'
			      }
			    )
			    Alert.alert(granted)
			    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
			    	ToastAndroid.show('Permission Granted', ToastAndroid.SHORT);
			    	
					SoundRecorder.start(SoundRecorder.PATH_CACHE + '/test.mp4')
				    .then(function() {
				        Alert.alert('started recording');
				    });		    	
			    } else {
			      ToastAndroid.show('Record Audio Permission Denied', ToastAndroid.SHORT);
			    }
			  } catch (err) {
			    console.warn(err)
			  }
	}

	recordFunction() {
		
		this.requestRecorderPermission();
		
	}
	

	render(){

		return (
			<View style = {styles.viewStyle}>
				<Header queTypeText = {'Rearrange the text and speak into the Mic.'} colorStyle = {styles.queTypeColorStyle}/>
				
				<View style = {{alignItems: 'center'}}>
					
					<View style = {styles.queViewStyle}>
						<Text style = {styles.queStyle}> I have packed four bottles of water. </Text>
					</View>
					<View style = {styles.ansViewStyle}>
						<TouchableOpacity onPress = {() => this.recordFunction()}>
							<Icon
			                name='mic'
			                size={130}
			                color='black' />
						</TouchableOpacity>
					</View>
				</View>

				<Footer />
			</View>
		);
	}
};

const styles = {
	viewStyle: {
		fontSize: 18,
		flex: 1,
	    backgroundColor: '#3ca4d9',
	    color: '#ffffff'
	},
	queTypeColorStyle: {
		backgroundColor: '#065b6d',
		borderColor: '#065b6d',
	},
	queViewStyle: {
		marginLeft: 20,
		marginRight: 20
	},
	queStyle: {
		fontSize: 25,
		color: '#ffffff',
	},
	ansViewStyle: {
		flexDirection: 'column',
		justifyContent: 'space-between',
		marginTop: 60,
		borderColor: '#e0c523',
		borderWidth: 1,
		borderRadius: 200,
		backgroundColor: '#e0c523',
		padding: 10
	}
};

export default QueMic;