import React, { Component } from 'react';
import {Text, View, TextInput, TouchableOpacity, Image, ToastAndroid} from 'react-native';
import Header from './Header';
import Footer from './Footer';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/AntDesign';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Sound from 'react-native-sound';
import answer from './../images/answer.mp3';


const QueSound = () => {
	const sound = new Sound(answer, null, (error) => {
		  if (error) {
		    Alert.alert(error);
		  }
	});


	return (
		<View style = {styles.viewStyle}>
			<Header queTypeText = {'Listen and choose the missing word(s).'} colorStyle = {styles.queTypeColorStyle}/>

			<View style = {{alignItems: 'center'}}>

				<View style = {styles.queViewStyle}>
					<Text style = {styles.queStyle}> We saw _____ animals at the zoo.</Text>
				</View>
				<View style = {styles.soundViewStyle}>
					<TouchableOpacity style = {styles.iconStyle} onPress = {() => sound.play()}>
						<Icon
			                name='sound'
			                size={50}
			                color='black' />
					</TouchableOpacity>
					<TouchableOpacity style = {styles.iconStyle}>
						<Ionicon
			                name='md-musical-notes'
			                size={50}
			                color='black' />
					</TouchableOpacity>
				</View>
				<View style = {styles.ansViewStyle}>
					<View style = {styles.ansLineStyle}>
						<TouchableOpacity style = {styles.buttonStyle} onPress={() => ToastAndroid.show('Wrong Answer', ToastAndroid.SHORT)}>
							<Text style = {styles.buttonTextStyle}>Very</Text>
						</TouchableOpacity>
						<TouchableOpacity style = {styles.buttonStyle} onPress = {() => Actions.queRearr()}>
							<Text style = {styles.buttonTextStyle}>Many</Text>
						</TouchableOpacity>
					</View>
					<View style = {styles.ansLineStyle}>
						<TouchableOpacity style = {styles.buttonStyle} onPress={() => ToastAndroid.show('Wrong Answer', ToastAndroid.SHORT)}>
							<Text style = {styles.buttonTextStyle}>Any</Text>
						</TouchableOpacity>
						<TouchableOpacity style = {styles.buttonStyle} onPress={() => ToastAndroid.show('Wrong Answer', ToastAndroid.SHORT)}>
							<Text style = {styles.buttonTextStyle}>For</Text>
						</TouchableOpacity>
					</View>
				</View>
			</View>

			<Footer />
		</View>
	);

};

const styles = {
	viewStyle: {
		fontSize: 18,
		flex: 1,
	    backgroundColor: '#95c963',
	    color: '#ffffff'
	},
	queTypeColorStyle: {
		backgroundColor: '#065b6d',
		borderColor: '#065b6d',
	},
	queViewStyle: {
		marginLeft: 20,
		marginRight: 20
	},
	queStyle: {
		fontSize: 25,
		color: '#ffffff',
	},
	soundViewStyle: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: 10,
		borderColor: '#3b8168',
		borderWidth: 1,
		borderRadius: 30,
		backgroundColor: '#3b8168',
		padding: 5
	},
	iconStyle: {
		paddingLeft: 5,
		paddingRight: 5
	},
	ansViewStyle: {
		flexDirection: 'column',
		justifyContent: 'space-between',
		marginTop: 30
	},
	ansLineStyle: {
		flexDirection: 'row',
		marginBottom: 20
	},
	buttonTextStyle: {
		fontSize: 22,
		color: '#3b8168',
		padding: 10,
		textAlign: 'center'
	},
	buttonStyle: {
		backgroundColor: '#ffffff',
		borderWidth: 1,
		borderColor: '#ffffff',
		borderRadius: 10,
		height: 50,
		width: 100,
		marginLeft: 10,
		marginRight: 10,
		padding: 0
	}
};

export default QueSound;