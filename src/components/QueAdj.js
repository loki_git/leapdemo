import React from 'react';
import {Text, View, TextInput, TouchableOpacity, Image, ToastAndroid} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Header from './Header';
import Footer from './Footer';

const QueAdj = () => {
	return (
		<View style = {styles.viewStyle}>
			<Header queTypeText = {'Choose the missing word(s).'} colorStyle = {styles.queTypeColorStyle} />
			
			<View style = {{alignItems: 'center'}}>
				
				<View style = {styles.queViewStyle}>
					<Text style = {styles.queStyle}>The old man has not got _____ hair on his head.</Text>
				</View>
				<View style = {styles.ansViewStyle}>
					<View style = {styles.ansLineStyle}>
						<TouchableOpacity style = {styles.buttonStyle} onPress={() => ToastAndroid.show('Wrong Answer', ToastAndroid.SHORT)}>
							<Text style = {styles.buttonTextStyle}>Most</Text>
						</TouchableOpacity>
						<TouchableOpacity style = {styles.buttonStyle} onPress={() => Actions.queSound()}>
							<Text style = {styles.buttonTextStyle}>Any</Text>
						</TouchableOpacity>
					</View>
					<View style = {styles.ansLineStyle}>
						<TouchableOpacity style = {styles.buttonStyle} onPress={() => ToastAndroid.show('Wrong Answer', ToastAndroid.SHORT)}>
							<Text style = {styles.buttonTextStyle}>Because</Text>
						</TouchableOpacity>
						<TouchableOpacity style = {styles.buttonStyle} onPress={() => ToastAndroid.show('Wrong Answer', ToastAndroid.SHORT)}>
							<Text style = {styles.buttonTextStyle}>For</Text>
						</TouchableOpacity>
					</View>
				</View>
			</View>

			<Footer />
		</View>
	);
};

const styles = {
	viewStyle: {
		fontSize: 18,
		flex: 1,
	    backgroundColor: '#065b6d',
	    color: '#ffffff'
	},
	queTypeColorStyle: {
		backgroundColor: '#3ca4d9',
		borderColor: '#3ca4d9',
	},
	queViewStyle: {
		marginLeft: 20,
		marginRight: 20
	},
	queStyle: {
		fontSize: 25,
		color: '#ffffff',
	},
	ansViewStyle: {
		flexDirection: 'column',
		justifyContent: 'space-between',
		marginTop: 60
	},
	ansLineStyle: {
		flexDirection: 'row',
		marginBottom: 20
	},
	buttonStyle: {
		backgroundColor: '#ffffff',
		borderWidth: 1,
		borderColor: '#ffffff',
		borderRadius: 10,
		height: 50,
		width: 120,
		marginLeft: 20,
		marginRight: 20
	},
	buttonTextStyle: {
		fontSize: 22,
		color: '#065b6d',
		padding: 10,
		textAlign: 'center'
	}
};

export default QueAdj;