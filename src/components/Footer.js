import React from 'react';
import { View, Image } from 'react-native';

const Footer = () => {
	return (
		<View style = {styles.imageStyle}>
			<Image source = {require('./../images/rocket.png')} style = {{width:100, height: 130, opacity: 1}} />
		</View>
	);
};

const styles = {
	imageStyle: {
	    flex: 1,
	    justifyContent: 'flex-end',
	    alignItems: 'flex-end'
	}
};

export default Footer;