import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import Login from './Login';
import QueAdj from './QueAdj';
import QueRearr from './QueRearr';
import QueMic from './QueMic';
import QueSound from './QueSound';

const RouterComponent = () => {
	return (
		<Router>
			<Scene key = "root">
				<Scene key="login" component={Login} hideNavBar = {true}/>
				<Scene key="queAdj" component={QueAdj} title="Unit:Adjective" hideNavBar = {true}/>
				<Scene key="queRearr" component={QueRearr} title="Unit:Adjective" hideNavBar = {true}/>
				<Scene key="queMic" component={QueMic} title="Unit:Adjective" hideNavBar = {true} />
				<Scene key="queSound" component={QueSound} title="Unit:Adjective" hideNavBar = {true} />
			</Scene>
		</Router>
	);
};

export default RouterComponent;