import React from 'react';
import {Text, View, TextInput, TouchableOpacity, Image, Dimensions} from 'react-native';
import { Button, CheckBox} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';

const Login = () => {
	const dimensions = Dimensions.get('window');
	const imageWidth = dimensions.width;

	return (
		<View>
	        
	        <View style = {{marginTop: 100}}>
	          <Image source = {require('./../images/LEAP_FINAL_COLORED.png')} style = {{height:120, width: imageWidth}} />
	        </View>

	        <View style = {styles.viewStyle}>

				<View style = {styles.inputIconStyle}>
					<Icon
		                name='user'
		                size={20}
		                color='black' />
					<TextInput
						style = {styles.inputStyle}
						placeholder = "Username" />
				</View>
				<View style = {styles.inputIconStyle}>
					<Icon
		                name='lock'
		                size={20}
		                color='black' />
					<TextInput style = {styles.inputStyle} placeholder = "Password" />
				</View>
				<View style = {{flexDirection: 'row', justifyContent: 'space-between'}}>
					<CheckBox title = 'Remember Me' value = '' />
					<Text style = {{width: null, marginTop:15}}>Forgot Password?</Text>
				</View>
				<View style = {{marginTop:10, alignItems: 'center'}}>
					<TouchableOpacity style = {styles.buttonStyle} onPress = {() => Actions.queAdj()}>
						<Text style = {styles.buttonTextStyle}>Login</Text>
					</TouchableOpacity>
				</View>
				
			</View>
			
		</View>
	);
};

const styles = {
	viewStyle: {
		fontSize: 18,
		// width: 250,
		// flex: 1,
		flexDirection: 'column',
	    justifyContent: 'center',
	    marginTop: 50,
	    marginLeft: 30,
	    marginRight: 30
	},
	inputStyle: {
		flex: 1
    },
    inputIconStyle: {
	    flexDirection: 'row',
	    justifyContent: 'flex-start',
	    alignItems: 'center',
	    backgroundColor: '#fff',
	    borderColor: '#d0d0d0',
		borderWidth: 1,
		marginBottom: 5,
		paddingLeft: 2
	},
	buttonStyle: {
		justifyContent: 'center',
		backgroundColor: '#3b8168',
		borderWidth: 1,
		borderColor: '#3b8168',
		borderRadius: 5,
		height: 30,
		width: 120,
	},
	buttonTextStyle: {
		color: '#ffffff',
		textAlign: 'center'
	}

};

export default Login;